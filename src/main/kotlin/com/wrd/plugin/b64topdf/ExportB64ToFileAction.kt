package com.wrd.plugin.b64topdf

import com.intellij.notification.Notification
import com.intellij.notification.NotificationType
import com.intellij.notification.Notifications
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class ExportB64ToFileAction : AnAction() {
    val notificationGroup = "base64-to-file"
    val notificationTitle = "<strong>Base64 to File Plugin:</strong>"
    val notificationInfoMsg = "File saved successfully"
    val notificationErrorMsg = "Something went wrong while writing the file:"

    override fun actionPerformed(e: AnActionEvent) {
        val currentProject = e.project
        val dialog = ExportB64TopFileDialog(currentProject)

        if (dialog.showAndGet()) {
            val filename = dialog.getFileName()
            val saveToPath = dialog.getSaveToPath()
            val fileNameAndLocation = "${saveToPath}/${filename}"
            val input = dialog.getInput()


            try {
                val decoded = Base64.getDecoder().decode(input)

                Files.write(Paths.get(fileNameAndLocation), decoded)
                Notifications.Bus.notify(Notification(notificationGroup, notificationTitle, "${notificationInfoMsg}, take a look ${fileNameAndLocation}", NotificationType.INFORMATION))

            } catch (e: IOException) {
                Notifications.Bus.notify(Notification(notificationGroup, notificationTitle, "${notificationErrorMsg} ${e.localizedMessage}", NotificationType.ERROR))
                e.printStackTrace()
            }
        }
    }
}