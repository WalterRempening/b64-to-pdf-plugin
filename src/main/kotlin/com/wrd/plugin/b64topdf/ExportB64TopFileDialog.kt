package com.wrd.plugin.b64topdf

import com.intellij.openapi.fileChooser.FileChooserDescriptorFactory
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.openapi.ui.TextBrowseFolderListener
import com.intellij.openapi.ui.TextFieldWithBrowseButton
import com.intellij.openapi.ui.ValidationInfo
import com.intellij.ui.components.JBScrollPane
import com.intellij.ui.components.JBTextField
import com.intellij.ui.layout.panel
import com.intellij.util.ui.JBDimension
import java.io.File
import javax.swing.JComponent
import javax.swing.JTextArea
import javax.swing.ScrollPaneConstants


class ExportB64TopFileDialog(project: Project?) : DialogWrapper(project) {
    private val fileNameTextField = JBTextField()
    private val descriptionTextArea = JTextArea()
    private var saveDirectoryField: TextFieldWithBrowseButton = TextFieldWithBrowseButton()

    init {
        title = "Export base64 encoded string to file"
        setOKButtonText("Export")
        init()
    }

    override fun createCenterPanel(): JComponent? {
        descriptionTextArea.lineWrap = true
        val descriptionPane = JBScrollPane(descriptionTextArea,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER)
                .apply {
                    minimumSize = JBDimension(200, 50)
                    preferredSize = JBDimension(250, 100)
                }

        val onlyFolderChooserDescriptor = FileChooserDescriptorFactory.createSingleFolderDescriptor()
        val textFolderListener = TextBrowseFolderListener(onlyFolderChooserDescriptor)
        saveDirectoryField = TextFieldWithBrowseButton()
        saveDirectoryField.addBrowseFolderListener(textFolderListener)
        saveDirectoryField.addActionListener {
            val path = saveDirectoryField.text
            val file = File(path)

            if (file.exists() || !file.isDirectory()) {
                myOKAction.isEnabled = false
                setErrorText("Invalid location")
            }

            myOKAction.isEnabled = true
        }

        val panel = panel {
            row("File name:") { fileNameTextField() }
            row("Save to path: ") { saveDirectoryField() }
            row("Input: ") { descriptionPane() }
        }.apply { preferredSize = JBDimension(500, 250) }

        return panel
    }

    override fun doValidate(): ValidationInfo? {
        super.doValidate()

        if (fileNameTextField.text.isBlank() || fileNameTextField.text.isNullOrEmpty()) {
            return ValidationInfo("File name cannot be empty", fileNameTextField)
        }

        if (saveDirectoryField.text.isBlank() || saveDirectoryField.text.isEmpty()) {
            return ValidationInfo("A path must be given", saveDirectoryField)
        }

        if (descriptionTextArea.text.isBlank() || descriptionTextArea.text.isNullOrEmpty()) {
            return ValidationInfo("Provide a base64 encoded string as input", descriptionTextArea)
        }

        return null
    }

    internal fun getSaveToPath(): String? {
        return saveDirectoryField.text
    }

    internal fun getFileName(): String {
        return fileNameTextField.text
    }

    internal fun getInput(): String {
        return descriptionTextArea.text
    }
}