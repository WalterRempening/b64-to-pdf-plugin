package com.wrd.plugin.b64topdf

import org.junit.Ignore
import org.junit.Test
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*


class ExportB64ToPdfActionTest {

    @Ignore
    @Test
    fun notReallyTesting_createPdf() {
        val filename = "test-sml"
        try {
            val encoded = Files.readAllBytes(Paths.get("C:\\AZ_DATEN\\$filename.input"))
            val decoded = Base64.getDecoder().decode(encoded)
            Files.write(Paths.get("C:\\AZ_DATEN\\$filename.pdf"), decoded)
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

}
